#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 900)
        self.assertEqual(j, 1000)

    def test_read5(self):   # tests if an imput is the same number
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)

    def test_read6(self):   # tests if i > j
        s = "300 40\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 300)
        self.assertEqual(j, 40)

    def test_read7(self):   # testing large numbers
        s = "100000 200000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100000)
        self.assertEqual(j, 200000)

    def test_read8(self):   # test more i = j
        s = "2 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 2)

    def test_read9(self):   # test more i = j
        s = "3 3\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3)
        self.assertEqual(j, 3)

    def test_read10(self):  # test more i = j
        s = "71 71\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 71)
        self.assertEqual(j, 71)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v,20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):  # testing i=j
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):  # testing i>j
        v = collatz_eval(300, 40)
        self.assertEqual(v, 128)

    def test_eval_7(self):  # tests large numbers
        v = collatz_eval(100000, 200000)
        self.assertEqual(v, 383)

    def test_eval_8(self):  # tests more i=j
        v = collatz_eval(2, 2)
        self.assertEqual(v, 2)

    def test_eval_9(self):  # tests more i = j
        v = collatz_eval(3, 3)
        self.assertEqual(v, 8)

    def test_eval_10(self): # tests more i = j
        v = collatz_eval(71, 71)
        self.assertEqual(v, 103)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print5(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print6(self):
        w = StringIO()
        collatz_print(w, 300, 40, 128)
        self.assertEqual(w.getvalue(), "300 40 128\n")

    def test_print7(self):
        w = StringIO()
        collatz_print(w, 100000, 200000, 383)
        self.assertEqual(w.getvalue(), "100000 200000 383\n")

    def test_print8(self):
        w = StringIO()
        collatz_print(w, 2, 2, 2)
        self.assertEqual(w.getvalue(), "2 2 2\n")

    def test_print9(self):
        w = StringIO()
        collatz_print(w, 3, 3, 8)
        self.assertEqual(w.getvalue(), "3 3 8\n")

    def test_print10(self):
        w = StringIO()
        collatz_print(w, 71, 71, 103)
        self.assertEqual(w.getvalue(), "71 71 103\n")


    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve2(self):
        r = StringIO("1 1\n300 40\n100000 200000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 1 1\n300 40 128\n100000 200000 383\n")

    def test_solve3(self):
        r = StringIO("2 2\n3 3\n71 71\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "2 2 2\n3 3 8\n71 71 103\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
